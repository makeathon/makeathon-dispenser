var io = require('socket.io');
//get node-module for arduino
//var five = require("johnny-five");

//define serialPort for communication with arduino connected on COM3
var SerialPort = require("serialport").SerialPort;
var serialport = new SerialPort("COM3");

function getMyBoard(){
	console.log("getMyBoard has been called");
}
//sets up the socket.io module.
exports.initialize = function(server) {
	//verbindet socket.io module mit dem Server
	io = io.listen(server);
	io.set('log level', false);
	
	// called, when serialport is started
	serialport.on('open', function(){
		console.log('Serial Port Opend');
	})

	io.sockets.on("connection", function(socket){
		//socket functions come here
		socket.on('turnDispenser',function(){
			serialport.write('1', function(err) {
			    if (err) {
			      return console.log('Error on write: ', err.message);
			    }
			    console.log('message written');
			  });
		});
	});
};


