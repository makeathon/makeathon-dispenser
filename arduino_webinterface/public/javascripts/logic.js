//sendet connection Anfrage an Server von dem aus die Fkt aufgerufen wurde

//var io = require('socket.io').listen(server);
//var io = require('socket.io-client');

// uncomment me for arduino
//var socket = io.connect('/');
// end of uncomment me for arduino

// Put event listeners into place
window.addEventListener("DOMContentLoaded", function() {

    var genderSwitch = $("[name='genderCheckbox']").bootstrapSwitch();
    var ageSlider = $("[name='ageSlider']").slider();
    var data = {};

    $('#profileBtn').click(function(){
        $("#profileSection").removeClass('hidden');
        $("#profileSection2").removeClass('hidden');
    });
    $('#judge1').click(function(){
        genderSwitch.bootstrapSwitch('state', true, true);
        ageSlider.slider('setValue', 44);
        data.name = "Gerhard Blahusch";
        $("#profileSection").addClass('hidden');
        $("#profileSection2").addClass('hidden');
    });
    $('#judge2').click(function(){
        genderSwitch.bootstrapSwitch('state', true, true);
        ageSlider.slider('setValue', 34);
        data.name = "Marcel Tilly";
        $("#profileSection").addClass('hidden');
        $("#profileSection2").addClass('hidden');
    });
    $('#judge3').click(function(){
        genderSwitch.bootstrapSwitch('state', true, true);
        ageSlider.slider('setValue', 47);
        data.name = "Karl-Heinz Wind";
        $("#profileSection").addClass('hidden');
        $("#profileSection2").addClass('hidden');
    });
    $('#judge4').click(function(){
        genderSwitch.bootstrapSwitch('state', true, true);
        ageSlider.slider('setValue', 40);
        data.name = "Philipp Wallner";
        $("#profileSection").addClass('hidden');
        $("#profileSection2").addClass('hidden');
    });
    $('#judge5').click(function(){
        genderSwitch.bootstrapSwitch('state', false, false);
        ageSlider.slider('setValue', 36);
        data.name = "Claudia Ehinger";
        $("#profileSection").addClass('hidden');
        $("#profileSection2").addClass('hidden');
    });
    $('#judge6').click(function(){
        genderSwitch.bootstrapSwitch('state', true, true);
        ageSlider.slider('setValue', 47);
        data.name = "Johann Glas";
        $("#profileSection").addClass('hidden');
        $("#profileSection2").addClass('hidden');
    });

    $('#loseWeightBtn').click(function(){
        data.goal = "loseWeight";
        $("#loseWeightBtn").addClass('active');
        $("#gainMuscleBtn").removeClass('active');
        $("#gainWeightBtn").removeClass('active');
    });
    $('#gainMuscleBtn').click(function(){
        data.goal = "gainMuscle";
        $("#loseWeightBtn").removeClass('active');
        $("#gainMuscleBtn").addClass('active');
        $("#gainWeightBtn").removeClass('active');
    });
    $('#gainWeightBtn').click(function(){
        data.goal = "gainWeight";
        $("#loseWeightBtn").removeClass('active');
        $("#gainMuscleBtn").removeClass('active');
        $("#gainWeightBtn").addClass('active');
    });

    $('#send').click(function(){

        if (!data.name)
            data.name = ""

        data.isMale = genderSwitch.bootstrapSwitch('state');
        if (data.isMale)
            data.gender = "man";
        else
            data.gender = "woman";
        console.log("Client is male:" +data.isMale);

        data.age = ageSlider.slider('getValue');
        console.log("Client is " + data.age + " years old.");

        if (data.goal == "loseWeight")
            data.goal = "who wants to lose weight";
        else if (data.goal == "gainMuscle")
            data.goal = "who wants to gain muscles";
        else if (data.goal == "gainWeight")
            data.goal = "who wants to gain weight";
        else
            data.goal = "";
        console.log(data.goal);

        $('#personString').text("Hi " + data.name + ". For a " + data.age + " year old " + data.gender + " " + data.goal + " we recommend the following recipe:");

        $("#section1").addClass('hidden');
        $("#section2").removeClass('hidden');

    });
}, false);


function turnDispenser(){
	socket.emit('turnDispenser');
}