Starting the chat application:
Install Node.js on your system.

0.1 connect Arduino to favorite USB port
1. upload serial_read_write_sample2 onto arduino
2. install all necessary node_modules which can be found in benötigte node_modules
	install with $ npm install <node_name>
2.1. edit line $ var serialport = new SerialPort("COM3"); --> COM3 to your USB port, where arduino is connected to
3. launch $ node app.js
4. call localhost:3000 in your favorite browser
5. enjoy switching on and off your arduino led

If you have already installed node, open the cmd.exe console.
Then direct to the path where this readme.txt is saved. (with command: cd c:\....\AlphaEmilChatApplication)
Then enter the following command to start the app: node app.js

After the app is started direct in your browser to "localhost:3000".d
You should see a site where your able to chat.
Just enter your text message at the bottom and click on send.
To see the effect of socket.io open the chat in two tabs/windows. 
Then type a message in one window and the magic from socket.io prints a message on the second window without reloading it.