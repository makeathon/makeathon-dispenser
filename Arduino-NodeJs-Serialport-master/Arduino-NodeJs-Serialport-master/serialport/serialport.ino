/*
  Writing to Serial Port
  Writes a digital input on pin 2 into the serial
  
  This example code is in the public domain.
 */

// digital pin 2 has a pushbutton attached to it.
int led = 13;
int incomingByte = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(13, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
        digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
        // send data only when you receive data:
        if (Serial.available()) {
           // read the incoming byte:
           incomingByte = Serial.read();
           digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
           delay(100);              // wait for a second
           digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
           delay(100);
           //say what you got:
           Serial.print("I received: ");
           Serial.println(incomingByte, DEC);
        }
        else{
           digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
           delay(500);              // wait for a second
           digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
           delay(500);
        }
    Serial.write(1);
    Serial.print("I received: ");
    delay(1000);
//  // read the input pin:
//  int buttonState = digitalRead(led);
//  // print out the state of the button into the serial port:
//  if(buttonState == HIGH){
//    Serial.write(1);
//  }else{
//    Serial.write(0);
//  }
//  // delay in between reads for stability
//  delay(100);        
}



