# Makeathon nutrifit #

Application for the Dispenser build for Makeathon @ Automatica 2016 by team Nutrifit. [Awarded with the 1st place!](http://makeathon.automatica-munich.com/) and [awarded as the coolest idea at the Makeathon](http://www.huffingtonpost.de/matthias-dworak/die-5-coolsten-projekte-b_b_10700710.html).

### What does it do? ###

Nutrifit hosts a webserver running a Bootstrap website. On the website you can configure your personal shake given personal information such as age and gender or your personal goal.
It will create a shake based on your profile.
If running with a connected Arduino the 'Shake!' button will turn a stepper motor to dispense some of the shake.

![IMG-20160624-WA0007.jpg.jpeg](https://bitbucket.org/repo/4aaKLB/images/1985884648-IMG-20160624-WA0007.jpg.jpeg)

### How to set up w/o Arduino ###

* cd into project folder ./arduino-webinterface.
* run $>npm install to install neccessary dependencies.
* run server with $>node app.js

### How to set up with Arduino ###

* Uncomment all code lines that are preceded with 
```
// Uncomment for arduino.
```
* Set your serial port (e.g. COM3)
* run server with $>node app.js