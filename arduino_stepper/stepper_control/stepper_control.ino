#include <AFMotor.h>


// arduino_stepper: lässt den Servometer um eine achtel Umdrehung drehen für jedes Zeichen, was an den Serial geschickt wird
// troubleshooting: - motor falsch angeschlossen -> readme; - motor dreht sich beim anschließen, durch ansteurung nicht mehr -> testen ob Änderung von parameter DOUBLE auf SINGLE funktioniert
//                   - motor soll sich anfangs nicht um 100 drehen ->  $ motor.step(100, FORWARD, SINGLE); auskommentieren
//                    - motor soll sich schneller/langsamer drehen -> setSpeed() parameter größer/kleiner setzen
//                      - zweiten Motor einbinden: relevante codeteile einkommentieren
//                        - motor verhält sich komplett wild -> externe Powerquelle / netzteil an arduino anschließen






// stepper runs on port 1 -> M1 and M2
// our motor (42 stepper motor 1.3A 40mm 17HD40005-22B 3D printer stepper motor)
// has 360° / 1.8° = 200 steps
AF_Stepper motor(200, 1);
//AF_Stepper motor2(200, 2); // auskommentieren für zweiten Motor

/* Use a variable called byteRead to temporarily store
   the data coming from the computer */
byte byteRead;

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Stepper test!");

  motor.setSpeed(40);  // * Umdrehungen pro Minute
  
  motor.step(100, FORWARD, SINGLE); 
  motor.release();
  
//  motor2.setSpeed(40);  // * Umdrehungen pro Minute
  
//  motor2.step(100, FORWARD, SINGLE); 
//  motor2.release();
  delay(1000);
}

void loop() {
  /*  check if data has been sent from the computer: */
  if (Serial.available()) {
    byteRead = Serial.read();
    // ersten Motor um achtel Umdrehung drehen
    if(byteRead == 1){
        motor.step(25, FORWARD, DOUBLE); 
    }
    //zweiten Motor um achtel Umdrehung drehen
//    if(byteRead == 2){
//        motor2.step(25, FORWARD, DOUBLE); 
//    }
  }
  
// ab hier offizielles Beispiel für Motor ansteuerung:

  // motor.stop(#steps, direction, steptype)
  // single = single coil activation
  // double = 2 coils are activated at once (for higher torque)
  // interleave = alternate betw. single and double for twice the resolution (half of the speed)
  // microstepping = pwm (puls width modulate) coils for smooth motion between steps
  // stepping commands are blocking and can be set free by release()
  //motor.step(100, FORWARD, SINGLE); 
//  motor.step(100, BACKWARD, SINGLE); 

//  motor.step(100, FORWARD, DOUBLE); 
//  motor.step(100, BACKWARD, DOUBLE);

//  motor.step(100, FORWARD, INTERLEAVE); 
//  motor.step(100, BACKWARD, INTERLEAVE); 

//  motor.step(100, FORWARD, MICROSTEP); 
//  motor.step(100, BACKWARD, MICROSTEP); 
}
