1. connect arduino to laptop via usb
2. start arduino firmware
3. open stepper_control.ino
4. upload to arduino

hardware:
1. connect motor control shield to arduino board
2. connect stepper moter via cables red (m1,+), blue (m1, gnd), green (m2, gnd), black (m2, +)
3. connect stepper to awesome food dispenser